import json
from requests import api
import speech_recognition as sr
import pyttsx3
import pywhatkit
import datetime
import wikipedia
import requests
import json
from selenium import webdriver
api_url = "http://api.openweathermap.org/data/2.5/weather?q=Istanbul,tr&APPID=76b9744795bc9874f5b3a5755f5f4ed1"
listener = sr.Recognizer()
engine =pyttsx3.init()
voices = engine.getProperty("voices")
engine.setProperty('voice',voices[0].id)
engine.setProperty('language', 'tr')
rate = engine.getProperty('rate')
engine.setProperty('rate', 125)
wikipedia.set_lang("tr")
engine.say("Merhaba")
engine.runAndWait()
api_url= "http://api.openweathermap.org/data/2.5/weather?q=London,uk&APPID=76b9744795bc9874f5b3a5755f5f4ed1"
def talk(text):
    engine.say(text)
    engine.runAndWait()
def take_command():
    try:
        with sr.Microphone() as source:
            voice = listener.listen(source) 
            command = listener.recognize_google(voice,language="tr-tr")
            command = command.lower()
            if "alexa" in command:
                command = command.replace("alexa","")
                print(command)
    except:
        pass
    return command

def run_alexa():
    command = take_command()
    print(command)
    
    if "saat" in command:
        time = datetime.datetime.now().strftime("%H:%M")
        print(time)
        talk("Saat" + time ) 
    if "çal" in command:
        song = command.replace("Çal","")
        talk("Caliyorum")
        pywhatkit.playonyt(song)
    elif "kim" in command:
        person = command.replace("kim","")
        info =wikipedia.summary(person,3)
        print(info)
        talk(info)
    elif "selam" in command:
        talk("Selam nasilsin")
    elif "nasılsın" in command:
        talk("İyiyim ya sen")
    elif "youtube" in command:
        browser=webdriver.Chrome()
        browser.get("https://www.youtube.com")
    elif "facebook" in command:
        browser=webdriver.Chrome()
        browser.get("https://www.facebook.com/")
    elif "whatsapp" in command:
        browser=webdriver.Chrome()
        browser.get("https://web.whatsapp.com/")
    elif "instagram" in command:
        browser=webdriver.Chrome()
        browser.get("https://www.instagram.com")
    elif "hava durumu" in command:
        pass
    elif "kapat" in command:
        talk("İyi Gunler Efendim")
        quit()
    elif "hava" in command:
       
        response = requests.get(api_url)
        jsonResponse = json.loads(response.text)
        sicaklik = str(int( jsonResponse["main"]["temp"] - 273) ) +" derece"
        nemorani= "Yüzde "+ str(jsonResponse["main"]["humidity"])
        talk("Bu gun hava durumu soyle")
        talk(sicaklik)
        talk(nemorani)

    else:
        talk("Anlamadim lutfen tekrar eder misin...")

while True:
    run_alexa()